jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();
    
    //Paraleex

    if(navigator.userAgent.match(/Trident\/7\./)) {
      document.body.addEventListener("mousewheel", function() {
        event.preventDefault();
        var wd = event.wheelDelta;
        var csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
      });
    }
    
    //Page Zoom

    document.documentElement.addEventListener('touchstart', function (event) {
     if (event.touches.length > 1) {
       event.preventDefault();
     }
    }, false);

    //Sticky Header
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            jQuery(".header").addClass("fixed-header");
        } else {
            jQuery(".header").removeClass("fixed-header");
        }
    });
    
    // Testimonial Slider
    
    jQuery('.testi-slider').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left"></i></a>',
      nextArrow: '<a class="slick-next"><i class="fa fa-angle-right"></i></a>'
    });
    
    // Client Logo Slider
    
    jQuery('.clientlogo').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="fa fa-angle-left"></i></a>',
      nextArrow: '<a class="slick-next"><i class="fa fa-angle-right"></i></a>'
    });
   
    
    //Paralex

    if(navigator.userAgent.match(/Trident\/7\./)) {
      document.body.addEventListener("mousewheel", function() {
        event.preventDefault();
        var wd = event.wheelDelta;
        var csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
      });
    }
    
});

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

// Upload Section
jQuery(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

jQuery(document).ready( function() {
    jQuery('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});



// Scroll

jQuery(window).on("load resize scroll", function (e) {
    var Header = jQuery("header").innerHeight();
    jQuery('.bannerBlock').css('margin-top', Header);
});

// Resize 

function mobileElements() {
    if (jQuery(window).width() < 767) {
        jQuery('.navbar-collapse').removeClass('show') ; 
        jQuery(".navbar-nav>li>a").attr({"data-toggle": "collapse","data-target":"#navbarSupportedContent"});
    }
};
mobileElements();
jQuery(window).on("resize", function (e) {
    mobileElements();
});

//Header Scroll

jQuery(document).on('click', '.nav-item', function (e) {
    var index = jQuery(this).attr("href");
    if (jQuery(index).length > 0) {
        jQuery('html,body').animate({
            scrollTop: jQuery(index).offset().top - 55
        }, 3500);
    }
});
jQuery(document).on('click', '.nav-item', function (e) {
    e.preventDefault();
    mobileElements();
    jQuery('li').removeClass('current-menu-item');
    jQuery(this).addClass('current-menu-item');
    var index = jQuery(this).find('a').text();
    var headerClass = jQuery('header').hasClass('stick');
    index = (index == "Contact us" ? 'Contactus' : index);
    if (jQuery('#' + index).length > 0) {
        if (headerClass) {
            jQuery('html,body').animate({
                scrollTop: jQuery('#' + index).offset().top - 43
            }, 3500);
        } else {
            if (jQuery(window).width() < 767) {
                jQuery('html,body').animate({
                    scrollTop: jQuery('#' + index).offset().top - 100
                }, 3500);
            } 
            else {
                jQuery('html,body').animate({
                    scrollTop: jQuery('#' + index).offset().top - 120
                }, 3500);
            }
        }
    }
});